# lyrics-bot

Телеграм бот для текстов песен

## запуск

```sh
$ git clone git@codeberg.org:snvmk/lyrics-bot
$ cd lyrics-bot
$ cat > .env <<!
> BOT_TOKEN="забери у BotFather"
> GENIUS_TOKEN="забери с https://genius.com/api-clients/new -> Generate Access Token"
> !
$ npm i
$ node .
```

## использование

открой личку с твоим ботом

кинь ему либо какой-либо текст (название, кусок текста..) или само аудио

или же включите в своем боте Inline Query и делайте в любом чате `@имя_бота запрос`
