import 'dotenv/config';
import { Telegraf, Markup } from 'telegraf';
import Genius from 'genius-lyrics';

const usage = ctx => ctx.reply('Кинь само аудио или название песни');
const keyboard = url =>
  Markup.inlineKeyboard([Markup.button.url('Перейти', url)]);
const search = async term => await genius.songs.search(term);
const getLyrics = async item => (await item?.lyrics()) ?? 'Ничё не нашел';

const genius = new Genius.Client(process.env.GENIUS_TOKEN);
const bot = new Telegraf(process.env.BOT_TOKEN)
  .start(usage)
  .help(usage)
  .on('audio', async ctx => {
    const { performer, title } = ctx.message.audio;
    const song = (await search(`${performer} ${title}`))[0];
    return ctx.reply(await getLyrics(song), keyboard(song.url));
  })
  .on('text', async ctx => {
    const song = (await search(ctx.message.text))[0];
    return ctx.reply(await getLyrics(song), keyboard(song.url));
  })
  .on(
    'inline_query',
    async ctx =>
      await ctx.answerInlineQuery(
        (
          await Promise.allSettled(
            (
              await search(ctx.inlineQuery.query ?? '')
            ).map(async item => {
              let lyrics = await getLyrics(item);
              if (lyrics.length > 4096) lyrics = `${lyrics.slice(0, 4093)}...`;
              return {
                type: 'article',
                title: item.title,
                id: `${item.id}`,
                thumb_url: item.thumbnail,
                description: item.artist.name,
                input_message_content: {
                  message_text: lyrics,
                },
                reply_markup: keyboard(item.url),
              };
            })
          )
        )
          .filter(p => p.status === 'fulfilled')
          .map(p => p.value)
          .slice(ctx.inlineQuery.offset || 0, 50)
      )
  )
  .catch(err => console.error(err));

bot.launch();
['SIGINT', 'SIGTERM'].map(sig => process.on(sig, () => bot.stop(sig)));
